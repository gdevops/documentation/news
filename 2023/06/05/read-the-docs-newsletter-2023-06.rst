.. index::
   pair: ReadTheDocs ; 2023-06

.. _read_2023_06_05:

=====================================================
2023-06-05 **Read the docs newsletter-june-2023**
=====================================================

- https://blog.readthedocs.com/newsletter-june-2023/


News and updates
====================

⚠️ A .readthedocs.yaml configuration file will be required for your
   future builds. Read more about this change in Migrate your project
   to .readthedocs.yaml configuration file v2.

✅️ Visiting a language slug of a project without specifying the version
   now redirects to the default version. For instance, /en/ redirects to /en/latest/.

🍿️ All the talks from Write the Docs Portland 2023, which we sponsored
   this year, are ready to watch: Open the new playlist on `Youtube <https://www.youtube.com/watch?v=EZJ0mk9Jj3s&list=PLZAeFn6dfHpneQPsDWa4OmEpgW4pNiaZ2>`_.

🐛️ Builds with multiline build.commands should now be working reliably.
   Thanks to everyone who helped out!

🐛️ sphinx-rtd-theme 1.2.1 has been released with an important bug fix
   that caused jQuery to not load in certain projects.

🔒️ We updated custom domain certificates to 1.2 as the minimum TLS version.

🔒️ A high-severity vulnerability has been fixed: Write access to projects
   via API V2 (/api/v2/project/* endpoints) for any logged-in user.


Awesome project of the month
=================================

The most recent addition to `Awesome Read the Docs Projects <https://github.com/readthedocs-examples/awesome-read-the-docs>`_ 🕶️
is `Ray’s documentation 🕶️ <https://docs.ray.io/>`_
