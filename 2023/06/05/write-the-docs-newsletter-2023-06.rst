.. index::
   pair: WriteTheDocs ; 2023-06

.. _write_2023_06_05:

=====================================================
2023-06-05 **Write the docs newsletter-june-2023**
=====================================================

- https://us6.campaign-archive.com/?e=cf18243cc7&u=94377ea46d8b176a11a325d03&id=53f6f2460d
- https://www.writethedocs.org/blog/newsletter-june-2023/


Docs for developers
======================

- https://docsfordevelopers.com/

::

    If developers are the superheroes of the software industry, then the
    lack of documentation is our kryptonite.

    Kelsey hightower

Public vs. private docs
==========================

- https://www.writethedocs.org/blog/newsletter-june-2023/#public-vs-private-docs

In the world of software documentation, it’s pretty common to have your
docs free and open to use.

But is that right for all products? This month, the community discussed
some of the pros and cons.

The main reason given for keeping docs private: the advantages of secrecy.

Some companies worry that open documentation allows competitors to copy
your ideas and designs.

And if you work on security or payment tools, exposing the details of how
your software works can make it easier for bad actors to try to attack
the tools.

However, some folks expressed skepticism about whether keeping docs
private is a very effective barrier.

The downsides of private docs were around two areas: friction and marketing.

Open docs are easier for your users to access.

If they have to remember login information, they may just give up and
file a support ticket instead.

And docs-as-marketing is a well-worn path: there’s the potential to impress
evaluators if the doc set looks useful and explains the product clearly,
with Stripe often used as the canonical example.

One last point is that it can be hard to have the conversation just based
on the pros and cons.

The decision to make documentation public or private often comes from
high up in the company and keeping docs private is usually an attempt to
protect the company.

So if you’re trying to change any minds, be aware that it’s a delicate
conversation!


Code samples: Comments and placeholder text
==============================================

- https://www.writethedocs.org/blog/newsletter-june-2023/#code-samples-comments-and-placeholder-text

