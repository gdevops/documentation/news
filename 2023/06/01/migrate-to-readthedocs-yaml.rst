.. index::
   pair: Configuration ; readthedocs.yaml

.. _readthedocs_2023_06_01:

================================================================================
2023-06-01 **Migrate your project to .readthedocs.yaml configuration file v2**
================================================================================

- https://blog.readthedocs.com/migrate-configuration-v2/
- https://preview.mailerlite.com/a8a8j1z9v8/2227025654572915141/e5f1/

Announce
============

We are announcing the deprecation of building without a configuration file
(.readthedocs.yaml) together with the deprecation of configuration file v1 as well.

Read the Docs will soon start requiring a .readthedocs.yaml configuration
file for all projects in order to build documentation successfully.

**We will stop creating a default configuration file behind the scenes**,
making assumptions for your projects, and automatically fixing problems
with project configurations.


Adding a configuration file
==============================

If you have a project on Read the Docs that is not using a .readthedocs.yaml
configuration file, you will need to create one as soon as possible to
continue building your project.

