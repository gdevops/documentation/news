.. index::
   pair: RSS ; Flux Web
   pair: RSS ; yasfb
   pair: yasb ; Yet Another Sphinx Feed Builder

.. _add_rss_2023_12_27:

===========================================================================================================================================
2023-12-27 **Ajout de Flux Web aux documentations sphinx**, utilisation du module sphinx-contrib/yasfb (Yet Another Sphinx Feed Builder)
===========================================================================================================================================

- https://github.com/sphinx-contrib/yasfb
- https://doughellmann.com/projects/yasfb/
- https://flus.fr/carnet/declarez-vos-flux.html
- https://flus.fr/carnet/pourquoi-vous-devriez-publier-un-flux-web.html


pyproject.toml
=================

- - https://github.com/sphinx-contrib/yasfb (Yet Another Sphinx Feed Builder)

Adding the yasb (Yet Another Sphinx Feed Builder) Python module.

.. code-block:: toml

	[tool.poetry.dependencies]
	python = "^3.12"
	Sphinx = "*"
	sphinx-material = "*"
	sphinx-copybutton = "*"
	sphinx-design = "*"
	yasfb = "*"


conf.py
============

We add 4 things:

1) templates_path = ["_templates"] in order to store the _templates/layout.html
   file
2) a link to the RSS XML file
3) this block of commands::

  	# https://github.com/sphinx-contrib/yasfb
	extensions.append("yasfb")
	feed_base_url = html_theme_options["base_url"]
	feed_author = "Scribe"

4) the |FluxWeb| substitution

.. code-block:: python

    templates_path = ["_templates"]  (1)

	html_theme_options = {
		"base_url": "http://linkertree.frama.io/pvergain",
		"repo_url": "https://framagit.org/linkertree/pvergain",
		"repo_name": project,
		"html_minify": False,
		"html_prettify": True,
		"css_minify": True,
		"repo_type": "gitlab",
		"globaltoc_depth": -1,
		"color_primary": "green",
		"color_accent": "cyan",
		"theme_color": "#2196f3",
		"nav_title": f"{project} ({today})",
		"master_doc": False,
		"table_classes": ["plain"],
		"nav_links": [
			{
				"href": "genindex",
				"internal": True,
				"title": "Index",
			},
			{
				"href": "http://linkertree.frama.io/noamsw/rss.xml", (2)
				"internal": False,
				"title": "RSS",
			},
		],
		"heroes": {
			"index": "Liens noamsw",
		},
	}
	# https://github.com/sphinx-contrib/yasfb (3)
	extensions.append("yasfb")
	feed_base_url = html_theme_options["base_url"]
	feed_author = "Scribe"

	rst_prolog = """
	.. |sphinx| image:: /images/documentation.png
	.. |FluxWeb| image:: /images/rss_avatar.webp  (4)
	"""

_templates/layout.html
===========================

- https://flus.fr/carnet/declarez-vos-flux.html

Declare the RSS file in the HTML header.


.. code-block:: jinja

	{% extends "!layout.html" %}
	{% block extrahead %}
	  <link rel="alternate"
		type="application/rss+xml"
		title="Documentation news"
		href="https://gdevops.frama.io/documentation/news/rss.xml" />
	{% endblock %}


index.rst
============

Adding the RSS avatar in the front page.


::

    |FluxWeb| `RSS <https://gdevops.frama.io/documentation/news/rss.xml>`_

.gitlab-ci.yml
=================

We use the cicirello/pyaction:latest docker image because the yasfb module
needs the git command.

.. code-block:: yaml

	#image: cicirello/pyaction:latest

	image: cicirello/pyaction:latest

	pages:
	  script:
	  - pip install -r requirements.txt
	  - sphinx-build -d _build/doctrees . _build/html
	  - mv _build/html public
	  artifacts:
		paths:
		- public
	  only:
	  - main
