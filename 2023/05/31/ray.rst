.. index::
   pair: beautidul doc; ray
   ! ray

.. _ray_2023_05_31:

================================================================
2023-05-31 ray **Ray Serve: Scalable and Programmable Serving**
================================================================

- https://docs.ray.io/en/latest/
- https://github.com/ray-project/ray
- https://docs.ray.io/en/latest/ray-core/actors.html#ray-remote-classes
- https://fosstodon.org/@readthedocs/110463989114312961
- https://github.com/readthedocs-examples/awesome-read-the-docs

The most recent addition to our `awesome projects list <https://github.com/readthedocs-examples/awesome-read-the-docs>`_ is Ray's #documentation 🕶️.

Ray is an open source project, consisting of several components that all
have their own section in a combined documentation set.


- https://fosstodon.org/@readthedocs/110463991357067125

The project is made with #Sphinx together with extensions from #executablebooks
and others.

For instance: sphinx-book-theme, MyST, sphinx-design,
sphinx-copybutton and many more 🎁 🎁 🎁


One of our favorite details is how Ray’s #documentation uses termynal.js
for animated and user-friendly command-line instructions terminal windows
with copy-paste support. 💅

And the source code for termynal.js is also user friendly! 🥇


- https://fosstodon.org/@readthedocs/110464006512732496

Finally, a big +1 to using tabular interfaces in #documentation  instructions
to let the user easily activate relevant content (and hide the irrelevant content). 💯

That’s it for now 👋

Know another awesome project? Interested in more inspiration?

Check out our Awesome Read the Docs Projects repository on GitHub:
https://github.com/readthedocs-examples/awesome-read-the-docs

