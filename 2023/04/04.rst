
.. _documentation_2023_04:

=======================
2023-04
=======================


2023-04-01 **CSS print et communautés de pratiques**
========================================================

- https://pretalx.jdll.org/jdll2023/talk/GLUXPY/

Paged.js (Julie Blanc & Julien Taquet) et WeasyPrint (Lucie Anglade & Guillaume Ayoub)

Discussions et échanges autour des outils CSS print et l'amélioration
des spécifications du W3C.

Paged.js (javascript) et WeasyPrint (Python) sont deux outils libres et
open source permettant de transformer un contenu HTML en PDF prêt pour
l'impression avec l'utilisation de CSS spécifique répondant aux standards du W3C.

Nous proposons un atelier de 2h sous forme de discussion et d'échanges
autour de nos pratiques et les collaborations possibles entre les
différentes personnes impliquées dans la conception et le développement
d'outils libre et open-source autour de CSS Print.

Notre but est notamment de discuter de l'importance des standards et de
la manière dont nous implémentons des spécifications CSS du W3C (qui ne
sont pas toujours simples à comprendre et avec des décisions parfois
contraires entre outils).

À partir de là nous chercherons à ouvrir des pistes pour travailler
ensemble à l'évolution de nos pratiques et pourquoi pas, l'évolution
des spécifications.


Guillaume Ayoub
----------------------

Comme j’adore écrire du Python et me perdre dans les spécifications du
web (chacun ses passions), j’ai participé à la création et au développement
de plusieurs bibliothèques libres comme Radicale ou CairoSVG.

Au sein de CourtBouillon, je propose aujourd’hui du support professionnel
sur WeasyPrint, un outil libre de génération automatique de documents.

Autre(s) intervention(s) de l'orateur :

- `L’écran et le papier, une histoire croisée de CSS print avec Paged.js et WeasyPrint <https://pretalx.jdll.org/jdll2023/talk/SJNWBU/>`_



Julie Blanc
--------------

Paged.js (javascript) et WeasyPrint (Python) sont deux outils libres et
open source permettant de transformer un contenu HTML en PDF prêt pour
l'impression avec l'utilisation de CSS spécifique répondant aux standards
du W3C.

Autre(s) intervention(s) de l'orateur :

- `L’écran et le papier, une histoire croisée de CSS print avec Paged.js et WeasyPrint <https://pretalx.jdll.org/jdll2023/talk/SJNWBU/>`_



Lucie Anglade
----------------

Professionnelle de la mise en page(s) du web à CourtBouillon, je suis
également développeuse de logiciels libres, amatrice de cuisine et toujours
partante pour discuter autour d’un verre !

J’organise aussi les Meetup Python de Lyon.




