.. index::
   pair: News ; Documentation

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/news/rss.xml>`_

.. _doc_news:

=========================
**Documentation News**
=========================

.. toctree::
   :maxdepth: 4

   2025/2025
   2024/2024
   2023/2023
   2022/2022
   2021/2021
   2020/2020
   2019/2019
   2018/2018
   2017/2017
   2016/2016
