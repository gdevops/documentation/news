

.. _mdn_2020_10_22:

=======================================================================================
2020-10-22 MDN Web Docs: Editorial strategy and community participation By Chris Mills
=======================================================================================

.. seealso::

   - https://hacks.mozilla.org/2020/10/mdn-web-docs-editorial-strategy-and-community-participation/




About Chris Mills
===================

.. seealso::

   - https://hacks.mozilla.org/author/cmillsmozilla-com/


Chris Mills is a senior tech writer at Mozilla, where he writes docs and
demos about open web apps, HTML/CSS/JavaScript, A11y, WebAssembly, and more.

He loves tinkering around with web technologies, and gives occasional
tech talks at conferences and universities. He used to work for Opera
and W3C, and enjoys playing heavy metal drums and drinking good beer.

He lives near Manchester, UK, with his good lady and three beautiful children.


New editorial strategy
========================

Our updated editorial strategy has two main parts: the creation of
content pillars and an editorial calendar.

The MDN writers’ team has always been responsible for keeping the MDN
web platform reference documentation up-to-date, including key areas
such as HTML, CSS, JavaScript, and Web APIs.

We are breaking these key areas up into “content pillars”, which we
will work on in turn to make sure that the significant new web platform
updates are documented **each month**.

.. note:: This also means that we can start publishing our Firefox
   developer release notes again, so you can keep abreast of what we’re
   supporting in each new version, as well as Mozilla Hacks posts to
   give you further insights into what we are up to in Firefox engineering.

We will also be creating and maintaining an editorial calendar in association
with our partners on the Product Advisory Board — and the rest of the
community that has input to provide — which will help us prioritize general
improvements to MDN documentation going forward.

For example, we’d love to create more complete documentation on important
web platform-related topics such as accessibility, performance, and security.

MDN will work with domain experts to help us update these docs, as well
as enlist help from you and the rest of our community — which is what
we want to talk about for the rest of this post.


Community call for participation
===================================

There are many day-to-day tasks that need to be done on MDN, including
moderating content, answering queries on the `Discourse forums <https://discourse.mozilla.org/c/mdn/learn/250>`_, and helping
to fix user-submitted content bugs. We’d love you to help us out with these tasks.

To this end, we’ve rewritten our Contributing to MDN pages so that it
is simpler to find instructions on how to perform specific atomic tasks
that will help burn down MDN backlogs.

The main tasks we need help with at the moment are:

- Fixing MDN content bugs — Our sprints repo is where people submit
  issues to report problems found with MDN docs. We get a lot of these,
  and any help you can give in fixing issues would be much appreciated.
- Help beginners to learn on MDN — Our Learn web development pages
  get over a million views per month, and have active forums where
  people go to ask for general help, or request that their assessments
  be marked.
  We’d love some help with answering posts, and growing our learning community.

We hope these changes will help revitalize the MDN community into an
even more welcoming, inclusive place where anyone can feel comfortable
coming and getting help with documentation, or with learning new
technologies or tools.

If you want to talk to us, ask questions, and find out more, join the
discussion on the MDN Web Docs chat room on Matrix.
We are looking forward to talking to you.

Other interesting developments
=================================

There are some other interesting projects that the MDN team is working
hard on right now, and will provide deeper dives into with future
blog posts.

We’ll keep it brief here.

Platform evolution — **MDN content moves to GitHub**
-------------------------------------------------------

.. seealso::

   - https://github.com/mdn/kuma
   - https://chat.mozilla.org/#/room/#general:mozilla.org
   - https://github.com/mdn/mdn-minimalist
   - https://hacks.mozilla.org/2020/10/mdn-web-docs-evolves-lowdown-on-the-upcoming-new-platform/

For quite some time now, the MDN developer team has been planning a
radical platform change, and we are ready to start sharing details of it.

In short, we are updating the platform to move from a Wiki approach with
the content in a MySQL database, to a JAMStack approach with the content
being hosted in a Git repository (codename: Project Yari).

This will not affect end users at all, but the MDN developer team and
our content contributors will see many benefits including a better
contribution workflow (via Github), better ways in which we can work
with our community, and a simplified, easier-to-maintain platform
architecture. We will talk more about this in the next blog post!

Web DNA 2020
===============

The 2019 `Web Developer Needs Assessment (Web DNA) <https://insights.developer.mozilla.org/>`_ is a ground-breaking
piece of research that has already helped to shape the future of the
web platform, with input from more than 28,000 web developers’ helping
to **identify the top pain points with developing for the web**.

The Web DNA will be run again in 2020, in partnership with Google,
Microsoft, and several other stakeholders providing input into the
form of the questions for this year.

We launched the survey on October 12, and this year’s report is due out
before the end of the year.

