.. index::
   pair: sphinx; geopandas
   ! Geopandas

.. _geopandas_2022:

=======================
2022-12-14 GeoPandas
=======================

- https://github.com/geopandas/geopandas
- https://github.com/geopandas/geopandas/graphs/contributors
- https://github.com/geopandas/geopandas/blob/main/doc/source/conf.py
- https://geopandas.org/en/stable/index.html

Description
============

GeoPandas is an open source project to make working with geospatial data
in python easier.

GeoPandas extends the datatypes used by pandas to allow spatial operations
on geometric types. Geometric operations are performed by shapely.

Geopandas further depends on fiona for file access and matplotlib for
plotting.


We want to highlight some things we love from their docs
==========================================================

- https://fosstodon.org/@readthedocs/109513470380125262


GeoPandas is an open source project to make working with #geospatial data
in #Python easier. @geopandas extends the datatypes used by pandas to
allow spatial operations on geometric types.

We want to highlight some things we love from their docs.
🤏 (small) 🧵


🏗️ Structure
=============

- https://fosstodon.org/@readthedocs/109513472671613136

🏗️ Structure: the index page presents the main 4 topics of the project:
Getting started, Documentation, About GeoPandas and Community.

Clicking on Documentation, opens 4 more categories: User Guide,
Advanced Guide, Examples, API Reference.

Just 2 clicks to get right there! 🌩️


🎨 Gorgeous gallery with examples
=======================================

- https://fosstodon.org/@readthedocs/109513474893365783


📚 Very detailed API reference
=================================

- https://fosstodon.org/@readthedocs/109513474893365783
- https://geopandas.org/en/stable/getting_started/introduction.html

📚 Very detailed API reference with all the parameters, along with usage
examples that you can quickly copy and paste without worrying about
the Python prompt!


We are excited to have them hosted on Read the Docs!
========================================================

- https://fosstodon.org/@readthedocs/109513478297564601

We are excited to have them hosted on Read the Docs! We hope their
documentation inspires you to improve your own docs and share them with the world!

Let us know which docs you like the most and we will highlight it on
our next episode ⚡
