

====================================================================================
2022-06-09  we’re excited to welcome Benjamin Balder Bach to our team (Readthedocs)
====================================================================================

- https://x.com/benjaoming
- https://keybase.io/benjaoming
- https://github.com/benjaoming
- https://x.com/djangowiki
- https://overtag.dk/v2/

Announce
==========

- https://x.com/benjaoming/status/1534894067083091968?s=20&t=CxkaEDcPC3UQv3MnLEpUMQ
- https://blog.readthedocs.com/newsletter-june-2022/

Really truly happy and thankful to be part of this amazing team, and
taking part in improving and maintaining one of the Open Source world's
most important online platforms 💗


Read the Docs newsletter - June 2022
========================================

We’re excited to welcome Benjamin Balder Bach to our team, joining as a
part-time contractor for now.

He’s a developer with a history of working as an Open Source maintainer
and event organizer in the Django community.

He has also previously contributed to Read the Docs and will be a wonderful
addition to the team.

We’re also excited to see people using our new `build.jobs feature <https://blog.readthedocs.com/user-defined-build-jobs/>`_
that we previously announced.

There are a lot of interesting ways to adapt the build process with this,
and we’ll continue to watch with interest how people are using it!

