
.. _new_rest_tutorials:

================================================
**3 new restructuredtext Tutorials**
================================================


sphinx-intro-tutorial
======================

- https://sphinx-intro-tutorial.readthedocs.io/en/latest/index.html

pradyunsg
===========

- https://pradyunsg.me/furo/reference/

thomas-cokelaer
================

- https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html
