
.. _announce_sphinx_api:

============================================================================================================
2022-05-11 **Generating beautiful Python API documentation with Sphinx AutoAPI by Antoine Beyeler**
============================================================================================================

- :ref:`sphinx_antoine_beyeler`
