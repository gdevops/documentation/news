.. index::
   pair: SVG; export
   pair: sphinx; cli-recorder

.. _rich_svg_export:

=======================================
2022-05-09 **Rich 12.4.1 SVG export**
=======================================


Rich 12.4.1 (This release has a major change to the SVG export)
==================================================================

- https://github.com/Textualize/rich/releases/tag/v12.4.1
- https://github.com/Textualize/rich/compare/v12.4.0...master
- https://github.com/Textualize/rich/commit/7519ba1846d48b10f031696067af13e7f2784e67
- https://github.com/Textualize/rich/commit/5c268f363dcba0ee482ac5eeb81dc1c99c1e36dd

This release has a major change to the SVG export.
It is now a simpler design, and will render outside of a browser


Airflow rich SVG export
---------------------------

- https://x.com/willmcgugan/status/1523606364517400578?s=20&t=HdAUvzdd9uxpPJCGKAkK_w
- https://x.com/jarekpotiuk/status/1523604537709187072?s=20&t=HdAUvzdd9uxpPJCGKAkK_w

.. figure:: images/will_mac_gugan_2.png
   :align: center


::

    .. raw:: html

        <div align="center">
          <img src="images/AirflowBreeze_logo.png"
               alt="Airflow Breeze - Development and Test Environment for Apache Airflow">
        </div>


    .. raw:: html

        <div align="center">
          <a href="https://youtu.be/4MCTXq-oF68">
            <img src="images/breeze/overlayed_breeze.png" width="640"
                 alt="Airflow Breeze - Development and Test Environment for Apache Airflow">
          </a>
        </div>


Rich SVG export is already getting traction
---------------------------------------------

- https://x.com/willmcgugan/status/1523387263257878531?s=20&t=HdAUvzdd9uxpPJCGKAkK_w

.. figure:: images/will_mac_gugan.png
   :align: center


Rich SVG export is already getting traction. Looking forward to seeing
it used in docs.

I’m planning on using it in Textual docs, so that “screenshots” never
get out of date.


sphinx conf.py
===================

- https://github.com/Textualize/rich/commit/7519ba1846d48b10f031696067af13e7f2784e67#diff-008dcb3426febd767787b1521f1fe33086313b927ea37eaab86df5fa88a51698

::


    html_css_files = [
        "https://cdnjs.cloudflare.com/ajax/libs/firacode/6.2.0/fira_code.min.css"
    ]


Rich-cli 1.8.0 (Rich-cli will now export portable SVGs, which can be loaded in editors)
===========================================================================================

- https://github.com/Textualize/rich-cli/releases/tag/v1.8.0


Rich-cli will now export portable SVGs, which can be loaded in editors.
Previously Rich SVGs would only render correctly within a browser.)
