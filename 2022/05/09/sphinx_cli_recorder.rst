.. index::
   pair: sphinx; cli-recorder

.. _sphinx_cli_recorder:

=======================================
2022-05-09 **Sphinx cli-recorder**
=======================================

sphinx cli recorder
======================

- https://docs.kai-tub.tech/sphinx_cli_recorder/intro.html
- https://docs.kai-tub.tech/sphinx_cli_recorder/intro.html#welcome


This Sphinx extension is a tool to allow you to easily automate the
recording process of CLI applications (without you having to leave your editor 🤯).

Suppose you are developing a neat CLI application, possibly with rich (get it?)
visual output.
In that case, you put blood, sweat, and tears into the development part
but do you want to put the same amount of effort into the documentation ?

Shouldn’t it be easy to show what your CLI application can do ?

If you record a terminal session to show how to interact with your tool,
you need to ensure that the recording is kept up-to-date and doesn’t break
with future updates.

Then you need to know how to upload the file and embed it into your documentation.
And all you want to do is to show something cool like:


