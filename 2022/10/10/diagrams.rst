

.. _diagrams_ref:

======================================================================
2022-10-10 **Diagrams as Code** by Robert Stephane
======================================================================

- :ref:`diagrams_python`


Tweet de Robert Stephane
=========================

- https://x.com/RobertStphane19/status/1579369586478440449?s=20&t=YoJLWy8UJf3h-kpjmdN1aw
- https://blog.stephane-robert.info/post/devops-diagram-as-code/


.. figure:: images/tweet_stephane_robert.png
   :align: center

Pour construire vos schémas, plutôt que de recourir à un éditeur externe,
pourquoi pas les générer à partir de lignes de code ?

Il y a la :ref:`librairie python diagrams <diagrams_python>` qui en plus est facile à prendre
en main.

Ça devient donc du **Diagrams as Code**. #devops

- https://blog.stephane-robert.info/post/devops-diagram-as-code/

Example
========

- :ref:`diagrams_example`
