
===================================
2022-11-21 **The Stories We Tell**
===================================

- https://mailchi.mp/f8c3e812625b/torah-tech-issue-15553365
- Flux Web RSS: https://us19.campaign-archive.com/feed?u=30eae9068398a251f4e180849&id=98ea64e095


.. figure:: images/torah.png
   :align: center

   https://mailchi.mp/f8c3e812625b/torah-tech-issue-15553365

The Stories We Tell
======================

What makes a good engineer? Is it all about how many lines of code they
write on a given day, or is there more to the story ?


In this week's Parshah, we read the story of how Avraham sent his servant
Eliezer to find a wife for his son Yitzchak.
Eliezer goes on to find Rivkah, Avraham's great-niece, and brings her
back to Avraham's home where she marries Yitzchak.


The Torah, quite uncharacteristically, goes into great detail in the story,
going as far as repeating many parts twice, once as they happened and
then again when Eliezer reports about them happening to Rivkah's family.

 
This level of detail is quite uncharacteristic of the Torah, which is
usually as terse as possible. The sages derived many laws from an extra
word or a superfluous letter, and here the Torah repeats a whole story twice.

The Medrash (Bereshit Rabbah 60:8) takes note of this and says...

Rabbi Acha said: the idle talk of our forefathers' servants is greater
than the Torah of their children. [After all] the story of Eliezer is
written and repeated over 2-3 pages, while [some of the laws of the
impurity of] creeping animals are derived from a superfluous word in
the scripture.

Obviously, the laws of the Torah are important but just as important
(if not more) are the context, the history, and the moral lessons we
learn from the stories in the Torah.

Without the stories, we lack the contextual framework needed to bring
the Mitzvot and Halachot to life and apply their relevance to our
day-to-day life.


In programming too, sure, it's the actual code that makes everything
run, but there's more to the story.

**Without the documentation, code comments, discussions in Slack threads
and GitHub issues, Jira tickets, and others, a lot of the context gets lost**. 


Sure, you can (hopefully) get a sense of what the code is doing by reading
the code, but **that doesn't tell you much about the whys**.

- Why were these decisions made?
- Why is this feature important?
- What's so  critical about this edge case?
- Why is there a whole code block that doesn't seem to do anything yet
  breaks everything when removed?

These are all questions that are answered by the conversations that took
place when the code was written.

**Let's make sure to document these conversations so that this vital context
doesn't get lost**.


Shabbat Shalom,
Ben, Batya, and Yechiel
