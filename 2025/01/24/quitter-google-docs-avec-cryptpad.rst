.. index::
   pair: Google docs ; cryptpad
   ! Quitter Google docs avec cryptpad


.. _cryptpad_2025_01_24:

==================================================
2025-01-24 **Quitter Google docs avec cryptpad**
==================================================

- https://cryptpad.org/


CryptPad is a collaborative office suite that is end-to-end encrypted 
and open-source.
