.. index::
   pair: ChatGPT ; Etudiant

.. _arthur_perret_2024_11_15:

======================================================================================
2024-11-15 **Guide de l’étudiant pour ne pas écrire avec ChatGPT** par Arthur Perret
======================================================================================

- https://www.arthurperret.fr/blog/2024-11-15-guide-etudiant-ne-pas-ecrire-avec-chatgpt.html


Mastodon announce
=====================

- https://social.sciences.re/@arthurperret/113482741187310951

OpenAI a publié aujourd'hui un “Student’s Guide to Writing with ChatGPT” 
qui commence par en suggérer l'utilisation pour formater citations et 
bibliographies. 

C'était clairement une provocation destinée à me faire écrire un 
“Student’s Guide to Not Writing with ChatGPT”, que voici donc :

En anglais : https://www.arthurperret.fr/blog/2024-11-14-student-guide-not-writing-with-chatgpt.html
En français : https://www.arthurperret.fr/blog/2024-11-15-guide-etudiant-ne-pas-ecrire-avec-chatgpt.html

Si vous avez des retours constructifs, je les prends avec plaisir.


Extraits
===========

- https://www.zotero.org/

Alors utilisez plutôt un logiciel de gestion bibliographique, tel que Zotero. 

Il s’occupera de vos citations et bibliographies de manière fiable, parce 
qu’il est programmé pour. I

l vous suffit de nettoyer les métadonnées des références au fur et à mesure 
que vous les collectez, et vos bibliographies ne contiendront jamais d’erreurs


Comme l’a récemment montré une étude australienne, ChatGPT ne sait pas 
résumer, seulement raccourcir. 

À l’heure actuelle, savoir résumer reste un savoir-faire humain. 

Un savoir-faire que vous pouvez acquérir en suivant un cours de résumé 
dans un cursus en sciences de l’information, un programme de formation 
continue, un cours en ligne, etc. 
(Je suis plutôt partisant de faire les choses dans l’autre sens mais 
plusieurs collègues me disent que sur le fond cette méthode du reverse 
outlining leur est très utile et qu’ils la conseillent à des étudiants.)


Need to known (https://needtoknow.fyi/)
=============================================

- https://needtoknow.fyi/

Si vous avez besoin de plus d’informations sur ce sujet, je vous recommande 
vivement de consulter le site Need To Know de Baldur Bjarnason. 

Il s’agit d’une synthèse accessible d’un état de l’art très fouillé sur 
les risques liés à l’utilisation de l’IA générative. 

C’est un excellent point de départ. (Et si vous connaissez des ressources 
similaires en français, je suis preneur.)
