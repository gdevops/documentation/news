.. index::
   pair: pandoc ; wasm

.. _pandoc_wasm:

===========================================
2024-11-15 **pandoc-wasm** by terrorjack
===========================================

- https://sns.terrorjack.com/@terrorjack
- https://github.com/tweag/pandoc-wasm


Mastodon announce
=====================

- https://fosstodon.org/@pandoc/113486515944267819


Pandoc compiled to Wasm (WebAssembly), which enables live conversions 
in the browser.

- Live demo: https://tweag.github.io/pandoc-wasm/
- Repository: https://github.com/tweag/pandoc-wasm

Amazing work by @terrorjack and the ghc-meta-wasm folks!


Live demo
==============

- https://tweag.github.io/pandoc-wasm/


Yuto Takahashi's Playground
=============================

https://y-taka-23.github.io/wasm-pandoc/
