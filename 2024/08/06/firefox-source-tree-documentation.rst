

.. _firefox_sphinx:

==========================================================
2024-08-06 **firefox-source documentation** with sphinx
==========================================================

- https://firefox-source-docs.mozilla.org/index.html
- https://firefox-source-docs.mozilla.org/devtools-user/index.html
- https://firefox-source-docs.mozilla.org/accessible/index.html
- https://firefox-source-docs.mozilla.org/taskcluster/index.html
- https://firefox-source-docs.mozilla.org/writing-rust-code/index.html
- https://firefox-source-docs.mozilla.org/build/buildsystem/glossary.html
- https://firefox-source-docs.mozilla.org/js/index.html
- https://firefox-source-docs.mozilla.org/devtools-user/index.html
- https://firefox-source-docs.mozilla.org/devtools-user/keyboard_shortcuts/index.html
