.. index::
   pair: Gosselin ; Markdown

.. _gosselin_2024_01_16:

===========================================================================================================
2024-01-16 **Augmenting the Markdown language for great Python Graphical Interfaces** by Vincent Gosselin
===========================================================================================================

- https://www.taipy.io/posts/augmenting-the-markdown-language-for-great-python-graphical-interfaces
- https://links.taipy.io/WebtoGitHub
- https://docs.streamlit.io/library/api-reference
- https://dash.plotly.com/minimal-app
- https://www.markdownguide.org/basic-syntax/

In the past few years, several Python packages have proposed Python API
to build web-based interfaces such as:

- `Plotly Dash <https://dash.plotly.com/minimal-app>`_
- `Streamlit <https://docs.streamlit.io/library/api-reference>`_
- And more recently `Taipy <https://links.taipy.io/WebtoGitHub>`_

However, one of the most underrated work has been with the creation by
the Taipy R&D team of an alternative API: an Augmented Markdown API for
Python developers.

Markdown is one of the most used markup languages: starting from a simple
text document, Markdown allows the generation of HTML pages with high
presentation quality, immediately deployable on a network.

For those of you not familiar with the markdown language, have a look
`here (Basic Syntax The Markdown elements outlined in the original design document) <https://www.markdownguide.org/basic-syntax/>`_

Based on the work of Dr. Neil Bruce [1], the Taipy team has extended the
very simple syntax of Markdown by allowing the writer to add tags that
would trigger the generation of a graphical interface element directly
in their content.
