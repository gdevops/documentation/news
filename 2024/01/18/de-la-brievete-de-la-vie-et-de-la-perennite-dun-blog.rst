.. index::
   pair: Ploum ; De la brièveté de la vie et de la pérennité d’un blog (2024-01-18)

.. _ploum_2024_01_18:

=====================================================================================
2024-01-18 **De la brièveté de la vie et de la pérennité d’un blog** par Ploum
=====================================================================================

- https://ploum.net/2024-01-18-perennite-dun-blog.html

Introduction
================

Cette année 2024 m’est particulièrement symbolique.

En octobre, je fêterai les 20 ans de ce blog et ne serai plus très loin
du million de mots publiés.

Vingt ans qui ont fait de ce blog un élément central et
constitutif de mon identité. Si je rêve d’être reconnu comme écrivain
voire développeur ou scientifique, je resterai toujours avant tout un
blogueur. Blogueur est d’ailleurs le premier qualificatif accolé à mon
patronyme dans les médias ou sur Wikipédia.

Ironiquement, l’année de ces 20 ans de blog a commencé avec la plus longue
indisponibilité que ce site ait jamais connue. Durant plusieurs jours,
ploum.net a été complètement retiré du réseau. En cause, une attaque
massive contre Sourcehut, mon hébergeur, forçant ce dernier à migrer en
catastrophe vers une nouvelle infrastructure.

`Informations à propos de l’attaque contre Sourcehut <https://outage.sr.ht/>`_

Si j’ai vécu la mise hors-ligne de mon blog comme une amputation, une perte
d’une partie de mon identité, je sais fort bien que cela ne me porte aucun
préjudice durable, que je ne perds aucune donnée. Ce genre d’incidents
font partie de la vie et je tiens à remercier l’équipe de Sourcehut pour
la transparence totale et le travail accompli ces derniers jours.

Cet événement m’a également fait prendre conscience à quel point l’œuvre
de toute une vie pouvait disparaitre rapidement, dans l’indifférence la
plus générale. Si j’espère que mes livres sur arbres morts continueront
toujours à tomber aléatoirement d’une étagère ou être découverts chez
des bouquinistes, mes écrits en ligne, eux, sont d’éphémères bouteilles à
la mer susceptibles de couler dans les abysses de l’oubli au moindre incident.

Cette question m’obsède depuis que je suis papa : comment faire en sorte
que mes écrits en ligne me survivent ?

Nom de domaine et indépendance
=======================================

- https://erdorin.org/quinze-ans-de-blogage/

J’espère que mon blog sera encore consultable **le jour inévitable où
Facebook, Medium et Youtube auront été relégués avec Myspace et Skyblog
dans l’armoire des souvenirs historiques du web**.

Et quand je vois `Alias fêter les 15 ans de son blog <https://erdorin.org/quinze-ans-de-blogage/>`_ , je pense ne pas être
le seul.
**Les blogs sont les archives à long terme du web**

`Quinze ans de blogage (erdorin.org) <https://erdorin.org/quinze-ans-de-blogage/>`_

Concrètement, je fuis toutes les plateformes propriétaires et je lie tous
mes écrits à mon propre nom de domaine : ploum.net. En fait, pour être
sûr, je dispose même de trois noms, sur trois premiers niveaux différents:
ploum.net, ploum.be, ploum.eu.

À court terme, cela peut être un mauvais calcul: les pages Facebook de mes amis
décédés sont encore en ligne alors que les domaines expirent rapidement. Je
dois donc préparer la transmission de ces domaines pour assurer qu’ils
puissent me survivre.

Simplicité et minimalisme
==========================

Outre le nom de domaine, la première cause de mise hors ligne d’un site est
généralement le manque de maintenance. Un Wordpress piraté, car une mise à
jour n’a pas été faite. Une base de données incompatible avec une nouvelle
version du CMS. Même les générateurs de sites statiques doivent être mis à
jour, avec leur dépendance, et sont parfois abandonnés par les développeurs.

Pour résoudre ce problème, mon blog est depuis décembre 2022 autogénéré et
ne produit que du HTML très simple, compatible avec la plupart des navigateurs
passés, présents, futurs et sans ambiguïté. Chaque page est complètement
indépendante et contient ses propres instructions CSS (42 lignes).

Vous pouvez sauver un article de mon blog depuis votre navigateur et le réouvrir
n’importe où, même sans connexion. Pas de JavaScript, pas de framework,
pas de chargement dynamique, pas de fonte, pas d’appel extérieur.
Ça parait extraordinaire de nos jours, mais c’est tellement simple à gérer que je
me demande encore pourquoi on se casse la tête à apprendre et maintenir
des couches de framework.
Penser la pérennité peut avoir des bénéfices immédiats !

Autre point: je prends le plus grand soin à ce que les URLs de mes posts ne
changent pas.
Un lien vers un article devrait rester valide aussi longtemps que le nom
de domaine existera.

`La dernière version de Ploum.net <https://ploum.net/2022-12-04-fin-du-blog-et-derniere-version.html>`_

Sources disponibles et distribuées
=========================================

Les sources de mon blog sont écrites au format Gemtext, le format utilisé
sur le réseau Gemini. La particularité de ce format est qu’il s’agit
essentiellement de texte pur. Aussi longtemps que nous aurons des ordinateurs,
ces fichiers pourront être lus.

Mes billets ainsi que le générateur Python pour créer les pages HTML sont
disponibles via Git, un outil décentralisé bien connu de tous les développeurs
et certainement appelé à exister pour les décennies qui viennent.

La commande::

    git clone https://git.sr.ht/~lioploum/ploum.net

vous donne accès à toutes les sources de mon blog, que vous pouvez générer
avec un simple "python publish.py".
Cela ne nécessite aucune dépendance particulière autre que Python3.

Mais que faire si sourcehut est indisponible ? Je me rends compte qu’il
est nécessaire que je pousse mes sources sur plusieurs miroirs.

Miroirs ?
=============

Parlons de miroirs justement. Une copie de mon site, version web et gemini,
est disponible sur rawtext.club.

- `Ploum.net sur Rawtext.club (http) <https://rawtext.club/~ploum/>`_
- `Ploum.net sur rawtext.club (gemini) <gemini://rawtext.club/~ploum/>`_

Mais ce miroir est « accidentel ». Rawtext.club est un petit serveur
géré par un passionné que je remercie chaleureusement. C’est grâce
à lui que j’ai pu explorer l’univers Gemini. Je ne peux lui imposer un
hébergement sérieux ni compter sur sa pérennité.

Sebsauvage me fait également l’immense honneur de maintenir une copie de
mes publications, pour le cas où je serais forcé de retirer du contenu:

- `Ploum sur streisand.me <https://sebsauvage.net/streisand.me/ploum/>`_

Pour le futur, je réalise aujourd’hui qu’il serait pertinent que je
trouve un hébergeur autre que Sourcehut, pouvant être synchronisé par Git
et offrant également un hébergement Gemini.
Je pourrais rediriger ploum.be ou ploum.eu vers ce serveur de secours.

Les mailing-lists
==================

Si j’avoue préférer le RSS pour mon usage personnel, force est de constater
que les mailing-listes offrent un avantage : vous gardez une copie de tous
les anciens billets dans votre boîte mail. Chaque nouvel abonné est donc,
sans le savoir, un nouvel archiviste de mes écrits.

- `Mailing-list en français <https://listes.ploum.net/mailman3/postorius/lists/fr.listes.ploum.net/>`_
- `Mailing-list en anglais <https://listes.ploum.net/mailman3/postorius/lists/en.listes.ploum.net/>`_

J’envoie d’ailleurs une version "text-only" (pas de HTML) sur une seconde
mailing-list dont les archives sont publiques. Défaut majeur : contrairement
aux mailing-listes précitées, ces archives text-only sont hébergées par…
Sourcehut, le même hébergeur que mon blog !

- `Mailing-list text-only en français <https://lists.sr.ht/~lioploum/fr>`_
- `Mailing-list text-only en anglais <https://lists.sr.ht/~lioploum/en>`_

Le futur
==============

Le protocole Internet (IP) a été conçu à une époque où on pensait que
le stockage serait toujours très coûteux, mais que la bande passante serait
essentiellement gratuite. Le principe d’IP est donc de transmettre aussi
vite que possible les paquets de données et de tout oublier instantanément.

En conséquence, les protocoles s’appuyant sur IP, comme Gemini et HTTPS,
sont particulièrement fragiles. La perte d’un seul serveur peut signifier
la disparition définitive de sites entiers.

De nouveaux protocoles ou de nouveaux usages sont nécessaires pour transformer
Internet depuis le simple "échange de données temps réels" en "archive
planétaire".
Cet usage d’archive planétaire va bien entendu à l’encontre des intérêts
financiers actuels qui souhaitent que vous payiez, directement
ou à travers la pub, à chaque fois que vous consultez le même contenu.

Le « copyright », comme son nom l’indique, cherche à empêcher toute
copie ! Mais, petit à petit, des solutions apparaissent, qu’elles soient
nouvelles comme IPFS ou qu’il s’agissent de réflexion sur une utilisation
de technologies préexistantes.

- `Présentation de IPFS sur Wikipédia <https://fr.wikipedia.org/wiki/InterPlanetary_File_System>`_
- `Low budget P2P content distribution with git (solderpunk) <gemini://zaibatsu.circumlunar.space/~solderpunk/gemlog/low-budget-p2p-content-distribution-with-git.gmi>`_

Tout cela est encore expérimental, mais je garde un œil et réfléchis à
rendre ploum.net disponible sur IPFS.

Le présent
=============

La pérennité des contenus en ligne est une quête complexe et longue
haleine.
Je vous invite à y penser pour vos propres créations.
Que voulez-vous qu’il reste de vous en ligne dans quelques années ?

Paradoxalement, nous laissons trop de traces involontaires (des données
personnelles, des commentaires écrits sous le coup de la colère, des critiques
de produits sur Amazon) et nous perdons trop facilement ces œuvres auxquelles
nous accordons de l’importance (combien de vidéos personnelles disparaitront
avec la fin inéluctable de Youtube ?).

J’ai moi-même le regret d’avoir perdu beaucoup de textes rédigés et
publiés un peu partout avant l’existence de ce blog ou cédant aux sirènes
de la mode, postés sur Google+, Facebook ou Medium.

**J’en ai retenu une leçon majeure : on ne sait a priori pas quels contenus
seront importants pour notre futur moi**.


.. _dater_les_contenus_ploum:

**Il est également primordial de dater les contenus. Avec l’année**
========================================================================

Je ne compte plus les fiches de notes que j’ai retrouvées, mais que je
peux ne raccrocher à rien, ne sachant même pas estimer à quelle année
voire à quel lustre le texte se rapporte.

Cette leçon est également ce qui motive ma tentative de pérennisation de
mes écrits : je n’ai pas l’impression de n’avoir jamais écrit quelque chose
qui mérite une préservation éternelle.

Mais un historien du futur pourrait peut-être un jour trouver dans les
écrits de ce blogueur obscur et oublié une information cruciale, glissée
par hasard dans un billet et lui permettant de comprendre certains
paradoxes de notre époque.

Pour cette mission, **la technologie qui semble la plus robuste, la plus
résistante pour traverser les siècles voire les millénaires reste le
livre**.
Livres que ma famille collectionne et accumule dans tous les coins de
notre logis. Des milliers d’auteurs, vivants ou morts, mondialement connus
ou obscurs, qui continuent chaque jour à nous parler !

Si vous souhaitez m’aider dans ma quête de pérennité, je vous invite à
acquérir, prêter, offrir ou abandonner sur un banc mes livres.

**Car lorsque les ordinateurs seront éteints, seuls continueront à nous bercer,
à nous parler les mots gravés sur le papier**, ces éphémères imaginaires
survivants à la prétention d’immortalité des corps décomposés.
