.. index::
   ! Semantic Line Breaks

.. _semantic_line_breaks:

===========================================
2024-01-26 **Semantic Line Breaks**
===========================================

- https://sembr.org/
- https://daringfireball.net/projects/markdown/
- http://docutils.sourceforge.net/rst.html
- http://asciidoc.org/

Preambule
===========

When writing text with a compatible markup language, add a line break after
each substantial unit of thought.

Introduction
===================

**Semantic Line Breaks** describe a set of conventions for using insensitive
vertical whitespace to structure prose along semantic boundaries.

Many lightweight markup languages, including `Markdown <https://daringfireball.net/projects/markdown/>`_, `reStructuredText <http://docutils.sourceforge.net/rst.html>`_
and `AsciiDoc <http://asciidoc.org/>`_, join consecutive lines with a space.

Conventional markup languages like HTML and XML exhibit a similar behavior
in particular contexts.
This behavior allows line breaks to be used as semantic delimiters, making
prose easier to author, edit, and read in source — without affecting the
rendered output.
