.. index::
   pair: gitlab pages ; Parallel Deployments

.. _gitlab_pages_2024_09_23:

======================================================================================================================================================
2024-09-23 **GitLab Pages features review apps and multiple website deployment** by Matthew Macfarlane and Janis Altherr 
======================================================================================================================================================

- https://about.gitlab.com/blog/2024/09/23/gitlab-pages-features-review-apps-and-multiple-website-deployment/
- https://docs.gitlab.com/ee/user/project/pages/#create-multiple-deployments
- https://about.gitlab.com/releases/2024/09/19/gitlab-17-4-released/#gitlab-pages-parallel-deployments-in-beta
- https://docs.gitlab.com/ee/user/project/pages/#parallel-deployments
- https://gitlab.com/gitlab-org/gitlab-pages
- https://docs.gitlab.com/ee/user/project/pages/
- https://gitlab.com/mmacfarlane
- https://gitlab.com/janis


.. warning:: beta for Premium, Ultimate only

Introduction
===============

`GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_ helps 
organizations reap the rewards of knowledge management, including **better 
collaboration and accessibility**. 

Learn how to use a new feature, Parallel Deployments.

GitLab Pages has long been a popular choice for hosting static websites, 
allowing users to showcase their projects, blogs, and documentation directly 
from their repositories.

Before GitLab 17.4, you could only have a single version of your GitLab 
Pages website. So you couldn’t preview your changes or have multiple versions 
of your website deployed simultaneously. 

Now, with a Premium or Ultimate license, you can do both!


How to deploy documentation for different versions of your product
========================================================================

The Parallel Deployments feature is also a useful tool if you maintain 
the documentation of multiple versions of your software simultaneously.

The below CI config will not only create a pages deployment when there 
is a commit to the default branch, but also for any commit to branches 
named “v1”, “v2,” or “v3”.

::

    pages:
      stage: deploy
      script:
        - ...
      variables:
        PAGES_PREFIX: "$CI_COMMIT_BRANCH" # Use the branch name by default
      pages:
        path_prefix: "$PAGES_PREFIX" # use whatever value is set in the variable
      environment:
        name: "Pages ${PAGES_PREFIX}"
        url: "${CI_PAGES_URL}/${PAGES_PREFIX}"
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
          variables:
            PAGES_PREFIX: '' # No prefix
        - if: $CI_COMMIT_BRANCH == 'v1'
        - if: $CI_COMMIT_BRANCH == 'v2'
        - if: $CI_COMMIT_BRANCH == 'v3'

By using the $CI_COMMIT_BRANCH variable as the path_prefix value, each 
of these branches will deploy their documentation to their own sub-path 
of your website:

- The branch named v1 has its docs published to <my-domain>/v1.
- The branch named v2 has its docs published to <my-domain>/v2.
- The branch named v3 has its docs published to <my-domain>/v3.

A new commit to one of these branches will then trigger a new deployment 
to its respective path, keeping the documentation of multiple versions up to date.

The Parallel Deployments feature is a significant upgrade to GitLab Pages, 
offering a more flexible and efficient way to manage your knowledge. 

Whether you're working on a small project or a large-scale site with 
multiple versions, this new capability will make your workflow smoother 
and more efficient.

2024-09-19 **GitLab Pages parallel deployments in beta**
=================================================================

- https://about.gitlab.com/releases/2024/09/19/gitlab-17-4-released/
- https://gitlab.com/groups/gitlab-org/-/epics/10914 (GitLab Pages Main Deployment / Parallel Deployments)


This release introduces Pages parallel deployments in beta. 

You can now easily preview changes and manage parallel deployments for 
your GitLab Pages sites. 

This enhancement allows for seamless experimentation with new ideas, so 
you can test and refine your sites with confidence. 

By catching any issues early, you can ensure that the live site remains 
stable and polished, building on the already great foundation of GitLab Pages.

Additionally, parallel deployments can be useful for localization when 
you deploy different language versions of an application or website.


