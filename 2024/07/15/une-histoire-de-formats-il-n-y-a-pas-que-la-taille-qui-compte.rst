

.. _sibaud_2024_07_15:

================================================================================================================================
2024-07-15 **Une histoire de formats : il n’y a pas que la taille qui compte** par Benoit Sibaud, patrick_g 
================================================================================================================================

- https://linuxfr.org/news/une-histoire-de-formats-il-n-y-a-pas-que-la-taille-qui-compte#toc-les-formats-mat%C3%A9riels-entre-co%C3%BBt-et-rangement

Auteurs : Benoît Sibaud et patrick_g
=====================================================

- https://linuxfr.org/users/oumph (Benoît Sibaud)
- https://linuxfr.org/users/patrick_g 



Une histoire de formats : il n’y a pas que la taille qui compte
=====================================================================


Dans cette nouvelle excursion dans le temps et dans l’espace du Transimpressux, 
nous allons rendre une rapide visite à Théotiste Lefevbvre (1798 - 1887) 
prote d’imprimerie et à quelques-uns de ses confrères ainsi que dans les 
magasins de quelques bibliothèques. 

Nous passerons aussi, un grand moment du côté de la Silicon Valley et de 
Redmond dans l’État de Washington, bien obligé puisqu’on parlera beaucoup 
de formats numériques, sans oublier d’aller dire bonjour à Donald Knuth, 
Tim Berners-Lee et John Gruber. 

On terminera notre exploration quelque part dans les archives numériques 
de la Bibliothèque nationale de France (BnF).


- https://linuxfr.org/news/une-histoire-de-formats-il-n-y-a-pas-que-la-taille-qui-compte#toc-les-formats-de-texte
