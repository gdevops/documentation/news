.. index::
   pair: breathe; 2019-06-03

.. _breathe_2019_06_03:

==============================================================================================
2019-06-03 **Clear, Functional C++ Documentation with Sphinx + Breathe + Doxygen + CMake**
==============================================================================================

- https://devblogs.microsoft.com/cppblog/clear-functional-c-documentation-with-sphinx-breathe-doxygen-cmake/


Writing good documentation is hard. Tools can’t solve this problem in
themselves, but they can ease the pain.

This post will show you how to use Sphinx to generate attractive,
functional documentation for C++ libraries, supplied with information
from Doxygen.

We’ll also integrate this process into a CMake build system so that we
have a unified workflow.

For an example of a real-world project whose documentation is built like
this, see `fmtlib <http://fmtlib.net/latest/>`_

