
.. _sphinx_materials_2021_07_23:

==========================================
2021-07-23 sphinx-material updates
==========================================

- https://github.com/bashtage/sphinx-material/commits?author=bashtage
- https://github.com/bashtage/sphinx-material/commit/f94eccf13e3e597f8007a326a2a845494f07f02c
