
.. _shared_theme_2021_03_06:

==================================================================================
2021-03-05 **Discuss shared theme infrastructure between EBP and other themes**
==================================================================================

- https://github.com/executablebooks/meta/issues/279


Currently there are several themes that have slight variations on the same structure:

- PyData Sphinx Theme - Maintained by the PyData community, in particular
  myself and @jorisvandenbossche (ping also @bollwyvl)

    - Sphinx Book Theme - Maintained by the EBP community (in particular
      @choldgraf, @mmcky, @AakashGfude, and @chrisjsewell) and dependent
      on the PST

- Furo - Maintained by @pradyunsg
- QuantEcon Theme (demo here) - Maintained by the QuantEcon community
  (in particular @DrDrij)
- Inspid Sphinx Theme - maintained by @mgeier

A lot of these themes share the same basic structure - a topbar, left
sidebar, middle content, right sidebar, and bottom nav/footer.

They also share many similar UI elements (eg., "click to expand subsections"
in the sidebar).

As a result there is a lot of duplication of similar work and UI/UX
happening across these themes.


.. figure:: archi_page.png
   :align: center
