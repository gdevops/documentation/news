
.. _ines_montani_2021_03_05:

============================================================================
2021-03-05 **Inès Montani on typing**  (https://x.com/_inesmontani)
============================================================================

.. seealso::

   - https://x.com/_inesmontani/status/1367835540867846150?s=20
   - https://github.com/pytest-dev/pytest/issues/8405
   - https://x.com/nedbat/status/1367801775189860353?s=20
   - https://github.com/pytest-dev/pytest/issues/8405




Inès Montani
===============


I've thought a lot about how to document type hints for @spacy_io
v3 – & how to avoid making the docs worse (like in the quoted example).

Compromise we've come up with: color for readability, simplify very
complex types, include links to relevant objects. https://spacy.io/api/entityrecognizer#init


.. figure:: ines_montani_on_typing.png
   :align: center

   https://x.com/_inesmontani/status/1367835540867846150?s=20


David
======

.. seealso::

   - https://github.com/pytest-dev/pytest/issues/8405#issuecomment-791680787

You're using the Sphinx autodoc extension, add autodoc_typehints =
"description" to docs/conf.py to get the hints next to the parameter/return
descriptions instead of in the signature.
