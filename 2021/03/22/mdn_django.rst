
.. mdn_2021_03_22:

===================================================================
2021-03-22 **How MDN’s site-search works**
===================================================================

.. seealso::

   - https://hacks.mozilla.org/2021/03/how-mdns-site-search-works/
   - https://jamstack.org/what-is-jamstack/



tl;dr:
=======

Periodically, the whole of MDN is built, by our Node code, in a **GitHub Action**.
A Python script bulk-publishes this to Elasticsearch.
Our Django server queries the same Elasticsearch via /api/v1/search.
The site-search page is a static single-page app that sends XHR requests
to the /api/v1/search endpoint. Search results’ sort-order is determined
by match and “popularity”


Jamstack’ing
=================

The challenge with `Jamstack” websites <https://jamstack.org/what-is-jamstack/>`_ is with data that is too vast
and dynamic that it doesn’t make sense to build statically.
Search is one of those. For the record, as of Feb 2021, MDN consists of
11,619 documents (aka. articles) in English.
Roughly another 40,000 translated documents. In English alone, there are
5.3 million words. So to build a good search experience we need to, as
a static site build side-effect, index all of this in a full-text
search database.
And Elasticsearch is one such database and it’s good. In particular,
Elasticsearch is something MDN is already quite familiar with because
**it’s what was used from within the Django app when MDN was a wiki**.

Note: MDN gets about 20k site-searches per day from within the site.
