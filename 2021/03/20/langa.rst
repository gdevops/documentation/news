
.. _langa_2021_03_20:

================================================================================================
2021-03-19 **“Writing a Static Site Generator in 30 Minutes”**  by https://x.com/llanga
================================================================================================

.. seealso::

   - https://x.com/llanga/status/1373351495605878784?s=20
   - https://www.youtube.com/watch?v=jNKFcB8hUu4
   - https://docs.python.org/3/library/typing.html#newtype
   - https://lukasz.langa.pl






Łukasz Langa
===============

Let's use Python 3.9 to write a blog generator from scratch and while
doing this, get a feel of how coding with type annotations feels like in 2021.
