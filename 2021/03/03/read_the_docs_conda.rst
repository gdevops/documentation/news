
.. _conda_2021_03_03:

===================================================================
2021-03-03 **Conda Support**
===================================================================

- https://docs.readthedocs.io/en/latest/guides/conda.html
- https://www.clinicalgraphics.com/en/


Conda Support
=================

Read the Docs supports Conda as an environment management tool, along
with Virtualenv.

Conda support is useful for people who depend on C libraries, and need
them installed when building their documentation.

This work was funded by `Clinical Graphics <https://www.clinicalgraphics.com/en/>`_
many thanks for their support of Open Source.
