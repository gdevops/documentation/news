
.. _django_dashboard_2021_03:

==========================================================================================================================
2021-03-21 For **django-sql-dashboard** I’ve decided to try out **Sphinx and Markdown** by https://x.com/simonw
==========================================================================================================================


- https://simonwillison.net/2021/Mar/21/django-sql-dashboard-widgets/
- https://noumenal.es/notes/sphinx/#restructuredtext-vs-markdown
- https://github.com/simonw/django-sql-dashboard/blob/main/docs/conf.py
- https://x.com/chrisj_sewell
- https://x.com/chrisj_sewell/likes
- https://x.com/chrisj_sewell/status/1372871265611046915?s=20
- :ref:`markdown_it_py`
- :ref:`myst_parser`



Introduction
==============

- https://myst-parser.readthedocs.io/en/latest/
- https://github.com/simonw/django-sql-dashboard


For django-sql-dashboard I’ve decided to try out Sphinx and Markdown
instead, using MyST—a Markdown flavour and parser for Sphinx.

I picked this because I want to add inline help to django-sql-dashboard,
and since it ships with Markdown as a dependency already (to power the
Markdown widget) my hope is that using Markdown for the documentation
will allow me to ship some of the user-facing docs as part of the
application itself.

But it’s also a fun excuse to try out MyST, which so far is working
exactly as advertised.

I’ve seen people in the past avoid Sphinx entirely because they preferred
Markdown to reStructuredText, so MyST feels like an important addition
to the Python documentation ecosystem.



.. figure:: django_dashboard.png
   :align: center
