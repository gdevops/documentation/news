
.. _multiversion_2021_03_26:

===========================================================================================================
2021-03-26 **Sphinx pour générer des documentations versionnées** par https://x.com/RobertStphane19
===========================================================================================================

.. seealso::

   - :ref:`ref_sphinx_multiversion`
   - https://x.com/RobertStphane19
   - https://blog.stephane-robert.info/post/sphinx-documentation-multi-version/
