
.. _paul_weveritt_2021_03_19:

================================================================================================
2021-03-19 **“Static Websites with Sphinx and Markdown”**  by https://x.com/paulweveritt
================================================================================================

.. seealso::

   - https://x.com/paulweveritt/status/1372868462528323585?s=20
   - https://x.com/choldgraf/status/1372923972753391618?s=20
   - https://x.com/chrisj_sewell/status/1372871265611046915?s=20
   - https://x.com/paulweveritt
   - https://x.com/choldgraf
   - https://x.com/chrisj_sewell
   - https://x.com/chrisj_sewell/likes
   - https://x.com/pythonwebconf
   - :ref:`markdown_it_py`
   - :ref:`myst_parser`




Paul Everitt
===============

.. seealso::

   - https://x.com/choldgraf/status/1372923972753391618?s=20


My talk got accepted at @PyCon 2021. “Static Websites with Sphinx and Markdown”.
Sphinx beyond docs, with MyST from @choldgraf and @chrisj_sewell
… preview next week @pythonwebconf

.. figure:: chris_paul.png
   :align: center

   https://x.com/choldgraf/status/1372923972753391618?s=20


Chris Sewell
==================

.. seealso::

   - https://x.com/chrisj_sewell/status/1372871265611046915?s=20


.. figure:: sewell.png
   :align: center

   https://x.com/chrisj_sewell/status/1372871265611046915?s=20
