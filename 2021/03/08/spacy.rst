
.. _spacy_2021_03_08:

=========================
2021-01-08 **spacy**
=========================

.. seealso::

   - https://spacy.io/
   - https://x.com/spacy_io
   - https://x.com/_inesmontani/status/1367835540867846150?s=20



Usage
======

.. seealso::

   - https://spacy.io/usage



Models
=======

.. seealso::

   - https://spacy.io/models


Whats new in V3
================

.. seealso::

   - https://spacy.io/usage/v3


https://spacy.io/usage/v3#features-types
===========================================

.. seealso::

   - https://spacy.io/usage/v3#features-types
