
.. _rachel_andrews_2021_01_19:

=========================================================================================
**Documenting things over at MDN by Rachel Andrew (https://x.com/rachelandrew)**
=========================================================================================

.. seealso::

   - https://x.com/rachelandrew
   - https://csslayout.news/issues/286
   - https://github.com/mdn/content
   - https://github.com/mdn/content#simple-changes
   - https://github.com/mdn/content#pull-request-etiquette
   - https://developer.mozilla.org/en-US/
   - https://developer.mozilla.org/en-US/docs/MDN/Contribute
   - https://developer.mozilla.org/en-US/docs/Web/API/CSS_Object_Model



Article
=========

I'm currently spending my time documenting things over at `MDN <https://developer.mozilla.org/en-US/>`_.

The whole project was moved to `GitHub just before Christmas <https://github.com/mdn/content>`_,
which means that contributors don't have to use the wiki to make edits - in fact
simple fixes can be done via the GitHub interface.

One of the neat things about the move to GitHub is that every time I pull
to get an up to date copy of the content, I see just how many people
have helped improve the docs.

It's a nice reminder of how many folk in the community want to help
make MDN documentation better.

If you ever spot an issue on MDN, or would like to spend a bit of time
improving or adding to the docs read the information about contributing.

If you raise an issue, or make a PR on HTML or CSS docs, it's quite
likely I'll end up reviewing it so don't be shy!

Everyone reviewing is very keen to encourage new people to get involved,
so you will be met by friendly people.

Writing docs is also an excellent way to learn about features.

I've spent quite a lot of time recently working on the CSS Object Model
references, and in creating examples to test my own knowledge before
documenting them, there have been many lightbulb moments.

I thought I knew my way around the CSSOM, but there is always something
to learn.

Rachel Andrew, CSS Layout News


Getting started
================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/MDN/Contribute/Getting_started
   - https://github.com/mdn/content/#setup
   - https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/GitHub


