
.. _unconference_2021_04_28:

=======================================================================
Mercredi 28 avril **read the docs unconference** by @juanluisback
=======================================================================

- https://github.com/writethedocs/www/issues/1516


Very productive unconference about Sphinx on #writethedocs!

Raw, unedited notes here https://github.com/writethedocs/www/issues/1516
