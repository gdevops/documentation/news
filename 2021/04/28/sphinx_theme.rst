
.. _sphinx_theme_2021_04_28:

=======================================================================
Mercredi 28 avril 2021 New sphinx_theme https://sphinx-themes.org/
=======================================================================

.. seealso::

   - https://sphinx-themes.org/
   - https://x.com/pradyunsg/status/1387296633017282560?s=20
   - https://github.com/sphinx-themes/sphinx-themes.org/issues


::

    I just deployed a revamp http://sphinx-themes.org.

    It should look much nicer, and should generally be easier to navigate!
