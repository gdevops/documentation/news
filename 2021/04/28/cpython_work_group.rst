
.. _python_doc_2021_04_28:

============================================================
Mercredi 28 avril 2021 Announcing the CPython Docs Workgroup
============================================================

.. seealso::

   - https://discuss.python.org/t/announcing-the-cpython-docs-workgroup/8412


Thanks to everyone for your patience as we worked to get the Docs Workgroup
off the ground.

As you may have remembered, Carol Willing and Ned Batchelder brought
forward the topic of CPython Documentation at last year’s Python Language Summit.

The topic has been further discussed at last year’s virtual Python core
dev’s sprint, and Carol has worked on the Docs WG charter .

Please see the group charter doc linked above to understand the purpose,
goals, group values, and guidelines of the Docs WG.

We’ve been working to move the Docs Workgroup forward. We’ll share more
details at next month’s Python Language Summit. For now, here’s some
high level information:

- The initial workgroup members are: Carol (@willingc), Mariatta (@mariatta),
  Ned Batchelder (@nedbat), and Julien Palard (@julien).
  Additionally, the Python Steering Council are permanent members of the WG.
- Work on this group will be organized on the python/docs-community
  GitHub repository. The charter information and meeting notes will be posted there.
- The Documentation category on Discourse will be the main communication
  channel between the WG and the wider Python community
- We will scheduling monthly meetings for the team to meet
- We’re still working on the logistics on taking in new members to the
  group. We’ll keep you updated!

Mariatta & Carol

