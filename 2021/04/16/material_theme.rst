
.. _material_2021_04_16:

====================================================================
2021-04-16 **A responsive Material Design theme for Sphinx sites**
====================================================================

.. seealso::

   - :ref:`ref_sphinx_material`
   - https://github.com/bashtage/sphinx-material/
   - https://x.com/iteration1/status/1383076250726457349
   - https://x.com/WillingCarol/status/1383087100749254656?s=20



Découverte le 16 avril 2021 grâce à:

- Carol Willing
- Karthik Gaekwad (https://x.com/iteration1)
- et humitos (https://x.com/reydelhumo)


Oh goodness no. I love Sphinx for many, many reasons.

The sophisticated content linking, autodoc, python http://conf.py flexibility.
Your work is making that even better.

Sometimes, though, the material mkdocs theme (insiders especially)
gives a **richer visual experience**



.. figure:: carol_willing_1.png
   :align: center

   https://x.com/WillingCarol/status/1383084933850099713?s=20



.. figure:: carol_willing.png
   :align: center

   https://x.com/WillingCarol/status/1383087100749254656?s=20


.. figure:: twitter_threads.png
   :align: center

   https://x.com/WillingCarol/status/1383089848894951432?s=20
