
.. _furo_2021_04_30:

============================================================
Vendredi 30 avril 2021 **sphinx furo theme**
============================================================

.. seealso::

   - https://pradyunsg.me/furo/
   - https://github.com/pradyunsg/furo
   - :ref:`furo_theme`
