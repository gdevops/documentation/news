
.. _documatt_2021_05_02:

=============================
2021-05-02 **documatt.com**
=============================

.. seealso:

   - https://documatt.com/
   - https://x.com/documattcom


Description
============


**Documatt** is web application under construction for writing and reading
beautiful books and documentation.

Under the covers it’s heavily based on Sphinx, Docutils and reStructuredText,
but hides their complexity to build professional and automated documentation.

It comes with stunning editor to assisted writing, powerful building and
linting the docs, and themes for excellent reading experience.


reStructuredText and Sphinx Reference
======================================

- https://restructuredtext.documatt.com/
- https://gitlab.com/documatt/sphinx-demo-project


Example based gentle reference of the reStructuredText and Sphinx syntax,
directives, roles and common issues.

It demonstrates almost all the markup making it also good for testing
Sphinx themes. Free and open-source.

source code: https://gitlab.com/documatt/sphinx-demo-project where issues and contributions are welcome
homepage: https://restructuredtext.documatt.com


