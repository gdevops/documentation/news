
.. _python_responsive_2021_05_09:

=============================================================================
2021-05-09 **python 3.11a0 responsive design** https://docs.python.org/3.11
=============================================================================

- https://x.com/bulat_olga
- https://x.com/bulat_olga/status/1391287580965081088?s=20
- https://docs.python.org/3.11

I'm so happy to see my PR for making the python docs theme responsive
finally merged and released!

You can view it in the docs for version 3.11: https://docs.python.org/3.11

Thank you, @sizeof, @willingc, @pradyunsg , FGuillet and septatrix,  for
your reviews and support along the way!


.. figure:: olga/olga_announce.png
   :align: center

   https://x.com/bulat_olga/status/1391287580965081088?s=20


.. figure:: olga/olga_announce_post.png
   :align: center
