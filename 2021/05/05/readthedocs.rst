
.. _readthedocs_2021_05_05:

=======================================
Read the Docs newsletter - May 2021
=======================================

- https://blog.readthedocs.com/newsletter-may-2021/


Upcoming features
====================

Anthony will keep working on releasing sphinx_rtd_theme 1.0, start getting
users to test our new user interface, and iron out our Data Processing Agreements.

On the EthicalAds side, David will be improving the KPI reporting for our
publishers, and onboarding Ra on the team along with Eric.

Eric is focused on onboarding our new hire on the EthicalAds team, finishing
a CZI grant proposal for funding in 2021-2022, and figuring out how the
team can handle growing from 5 to 8 folks in 2021!

Juan Luis will work with Eric on the new CZI proposal, continue discussing
with the Sphinx community about the new tutorial, and have more Customer
Development calls with existing users.

Manuel will continue improving our operations and deployment procedures,
make single sign-on discoverable by users, and release a new version of
sphinx-hoverxref compatible with newer versions of Sphinx and MathJax.

And finally, Santos will wrap up our new infrastructure and configuration
changes allowing users to install custom apt packages.

Considering using Read the Docs for your next Sphinx or MkDocs project?
Check out our documentation to get started!
